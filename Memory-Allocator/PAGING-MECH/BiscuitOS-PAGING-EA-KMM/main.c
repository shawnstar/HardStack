// SPDX-License-Identifier: GPL-2.0
/*
 * BiscuitOS MEMORY ALLOCATOR
 *
 * (C) 2024.01.10 BuddyZhang1 <buddy.zhang@aliyun.com>
 */
#include <linux/init.h>
#include <linux/kernel.h>

static int __init BiscuitOS_init(void)
{
	return 0;
}
__initcall(BiscuitOS_init);
