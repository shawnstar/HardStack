// SPDX-License-Identifier: GPL-2.0
/*
 * BiscuitOS Memory Allocator
 *
 * (C) 2024.01.10 BuddyZhang1 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#define DEV_PATH	"/dev/BiscuitOS-MM"
#define errExit(msg)	do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main()
{
	int fd;

	/* OPEN DEVICE  */
	fd = open(DEV_PATH, O_RDWR);
	if (fd < 0)
		errExit("OPEN FAILED.\n");

	/* RECLAIM */
	close(fd);

	return 0;
}
