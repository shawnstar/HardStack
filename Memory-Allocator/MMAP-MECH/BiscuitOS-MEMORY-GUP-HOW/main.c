// SPDX-License-Identifier: GPL-2.0
/*
 * GUP
 *
 * (C) 2024.01.04 BuddyZhang1 <buddy.zhang@aliyun.com>
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#define DEV_NAME		"BiscuitOS-GUP"

static ssize_t BiscuitOS_read(struct file *filp, char __user *buf,
			size_t len, loff_t *offset)
{
	char buffer[32];

	sprintf(buffer, "Hello BiscuitOS");

	if (copy_to_user(buf, buffer, strlen(buffer))) {
		printk(KERN_ERR "Unable copy data to user.\n");
		return -EINVAL;
	}

	return len;
}

static struct file_operations BiscuitOS_fops = {
	.owner		= THIS_MODULE,
	.read		= BiscuitOS_read,
};

static struct miscdevice BiscuitOS_drv = {
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= DEV_NAME,
	.fops	= &BiscuitOS_fops,
};

static int __init BiscuitOS_init(void)
{
	misc_register(&BiscuitOS_drv);
	return 0;
}

static void __exit BiscuitOS_exit(void)
{
	misc_deregister(&BiscuitOS_drv);
}

module_init(BiscuitOS_init);
module_exit(BiscuitOS_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("BiscuitOS <buddy.zhang@aliyun.com>");
MODULE_DESCRIPTION("BiscuitOS MMU Project");
