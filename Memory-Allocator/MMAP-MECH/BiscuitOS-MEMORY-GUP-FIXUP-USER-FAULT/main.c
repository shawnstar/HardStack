// SPDX-License-Identifier: GPL-2.0
/*
 * GUP: fixup_user_fault
 *
 * (C) 2024.01.03 <buddy.zhang@aliyun.com>
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/pagemap.h>

#define DEV_NAME		"BiscuitOS-GUP"
#define BISCUITOS_IO		0xBD
#define MM_GUP			_IO(BISCUITOS_IO, 0x00)

static long BiscuitOS_ioctl(struct file *filp,
                        unsigned int ioctl, unsigned long arg)
{
	bool locked = false;
	int r;

	switch (ioctl) {
	case MM_GUP:
		/* GUP */
		r = fixup_user_fault(current->mm, arg,
				     FAULT_FLAG_WRITE, &locked);
		if (r < 0) {
			printk("Fixup fault failed.\n");
			return r;
		}
		break;
	}
	return 0;
}

static struct file_operations BiscuitOS_fops = {
	.owner		= THIS_MODULE,
	.unlocked_ioctl	= BiscuitOS_ioctl,
};

static struct miscdevice BiscuitOS_drv = {
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= DEV_NAME,
	.fops	= &BiscuitOS_fops,
};

static int __init BiscuitOS_init(void)
{
	misc_register(&BiscuitOS_drv);
	return 0;
}

static void __exit BiscuitOS_exit(void)
{
	misc_deregister(&BiscuitOS_drv);
}

module_init(BiscuitOS_init);
module_exit(BiscuitOS_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("BiscuitOS <buddy.zhang@aliyun.com>");
MODULE_DESCRIPTION("BiscuitOS MMU");
