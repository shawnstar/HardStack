// SPDX-License-Identifier: GPL-2.0
/*
 * MMAP: MAP_LOCKED
 *
 * (C) 2023.12.20 BuddyZhang1 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

#define MAP_VADDR	(0x6000000000)
#define MAP_SIZE	(4096)

int main()
{
	void *mem;

	/* PREALLOC ANONYMOUS MEMORY */
	mem = mmap((void *)MAP_VADDR, MAP_SIZE,
		   PROT_READ | PROT_WRITE,
		   MAP_PRIVATE | MAP_ANONYMOUS | MAP_LOCKED,
		   -1,
		   0);
	if (mem == MAP_FAILED)
		exit(-1);

	/* ACCESS */
	*(char *)mem = 'B'; /* Write Ops Don't Trigger #PF */
	printf("MMAP: %#lx => %c\n", (unsigned long)mem, *(char *)mem);

	/* TRY TO SWAP OUT? */
	if (madvise(mem, MAP_SIZE, MADV_PAGEOUT) < 0)
		printf("SWAP OUT FAILED.\n");

	/* RECLAIM */
	munmap(mem, MAP_SIZE);

	return 0;
}
