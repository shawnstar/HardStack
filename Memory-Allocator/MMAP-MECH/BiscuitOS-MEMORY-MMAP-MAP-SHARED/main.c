// SPDX-License-Identifier: GPL-2.0
/*
 * MMAP: MAP_SHARED
 *
 * (C) 2023.12.17 BuddyZhang1 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

#define MAP_VADDR	(0x6000000000)
#define MAP_SIZE	(4096)

int main()
{
	void *mem;
	int pid;

	/* LAZYALLOC SHARED MEMORY */
	mem = mmap((void *)MAP_VADDR, MAP_SIZE,
		   PROT_READ | PROT_WRITE,
		   MAP_SHARED | MAP_ANONYMOUS,
		   -1,
		   0);
	if (mem == MAP_FAILED)
		exit(-1);

	/* ACCESS */
	*(char *)mem = 'B'; /* Write Ops Trigger #PF */
	printf("MMAP: %#lx => %c\n", (unsigned long)mem, *(char *)mem);

	/* SYS_FORK CREATE CHILD */
	pid = fork();

	if (pid == 0) {
		/* Child Write Ops, Trigger #PF */
		*(char *)mem = 'D';
	} else {
		char ch;

		/* Father sleep */
		sleep(1);
		/* Read Ops, Don't Trigger #PF */
		ch = *(char *)mem;
		printf("MMAP: %#lx => %c\n", (unsigned long)mem, ch);
	}

	/* RECLAIM */
	munmap(mem, MAP_SIZE);

	return 0;
}
