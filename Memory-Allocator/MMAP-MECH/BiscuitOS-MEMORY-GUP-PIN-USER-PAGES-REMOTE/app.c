// SPDX-License-Identifier: GPL-2.0
/*
 * GUP: pin_user_pages_remote 
 *
 * (C) 2024.01.03 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define DEV_PATH	"/dev/BiscuitOS-GUP"
#define MAP_VADDR	(0x6000000000)
#define MAP_SIZE	0x2000
#define BISCUITOS_IO	0xBD
#define MM_GUP		_IO(BISCUITOS_IO, 0x00)
#define MM_PID		_IO(BISCUITOS_IO, 0x01)
#define errExit(msg)	do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main()
{
	int fd, pid;
	char *mem;

	/* OPEN FILE */
	fd = open(DEV_PATH, O_RDWR);
	if (fd < 0)
		errExit("OPEN FAILED.\n");

	/* ANONYMOUS MEMORY */
	mem = (char *)mmap((void *)MAP_VADDR, MAP_SIZE,
			  PROT_READ | PROT_WRITE,
			  MAP_PRIVATE | MAP_ANONYMOUS |
			  MAP_POPULATE,
			  -1,
			  0);
	if (mem == MAP_FAILED)
		errExit("MAP FAILED.\n");

	pid = fork();

	if (pid == 0) {
		/* SON PRIVATE WRITE */
		*mem = 'D'; /* COW */

		/* SWAP OUT */
		madvise(mem, MAP_SIZE, MADV_PAGEOUT);
		sleep(2);

		/* SON READ */
		printf("SON %s\n", mem);
	} else {
		sleep(1); /* SON RUN FIRST */

		/* GUP TO SON PID  */
		if (ioctl(fd, MM_PID, (unsigned long)pid) < 0)
			errExit("IOCTL PID FAILED.\n");

		/* GUP TO VADDR  */
		if (ioctl(fd, MM_GUP, (unsigned long)mem) < 0)
			errExit("IOCTL VAD FAILED.\n");
	}

	/* RECLAIM */
	munmap(mem, MAP_SIZE);
	close(fd);

	return 0;
}
