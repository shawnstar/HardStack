// SPDX-License-Identifier: GPL-2.0
/*
 * GUP: MM-POPULATE
 *
 * (C) 2023.01.09 BuddyZhang1 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

#define MAP_VADDR	0x6000000000
#define MAP_SIZE	0x1000

int main()
{
	void *mem;

	/* PREALLOC */
	mem = mmap((void *)MAP_VADDR, MAP_SIZE,
		   PROT_READ | PROT_WRITE,
		   MAP_PRIVATE | MAP_ANONYMOUS |
		   MAP_POPULATE,
		   -1,
		   0);

	/* WRITE OPS. Don't Trigger #PF */
	*(char *)mem = 'D';
	printf("GUP-MM: %#lx -> %c\n", (unsigned long)mem, *(char *)mem);

	/* RECLAIM */
	munmap(mem, MAP_SIZE);

	return 0;
}
