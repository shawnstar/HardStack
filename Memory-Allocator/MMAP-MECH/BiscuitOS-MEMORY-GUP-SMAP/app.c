// SPDX-License-Identifier: GPL-2.0
/*
 * GUP: SMAP
 *
 * (C) 2024.01.04 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define DEV_PATH	"/dev/BiscuitOS-GUP"
#define MAP_VADDR	(0x6000000000)
#define MAP_SIZE	(4096)
#define errExit(msg)	do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main()
{
	char *mem;
	int fd;

	/* OPEN FILE */
	fd = open(DEV_PATH, O_RDWR);
	if (fd < 0)
		errExit("OPEN FAILED\n");

	/* LAYZALLOC ANONYMOUS MEMORY */
	mem = (char *)mmap((void *)MAP_VADDR, MAP_SIZE,
			   PROT_READ | PROT_WRITE,
			   MAP_PRIVATE | MAP_ANONYMOUS,
			   -1,
			   0);
	if (mem == MAP_FAILED)
		errExit("MMAP FAILED\n");

	read(fd, mem, 16);
	printf("GUP: %#lx => %c\n", (unsigned long)mem, *mem);

	/* RECLAIM */
	close(fd);

	return 0;
}
