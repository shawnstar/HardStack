// SPDX-License-Identifier: GPL-2.0
/*
 * GUP: LOOGTERM
 *
 * (C) 2024.01.09 <buddy.zhang@aliyun.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define DEV_PATH	"/dev/BiscuitOS-GUP"
#define MAP_VADDR	(0x6000000000)
#define MAP_SIZE	0x2000
#define BISCUITOS_IO	0xBD
#define MM_GUP		_IO(BISCUITOS_IO, 0x00)
#define errExit(msg)	do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main()
{
	char *mem;
	int fd;

	/* OPEN FILE */
	fd = open(DEV_PATH, O_RDWR);
	if (fd < 0)
		errExit("OPEN FAILED.\n");

	/* LAZYALLOC MEMORY */
	mem = (char *)mmap((void *)MAP_VADDR, MAP_SIZE,
			  PROT_READ | PROT_WRITE,
			  MAP_PRIVATE | MAP_ANONYMOUS,
			  -1,
			  0);
	if (mem == MAP_FAILED)
		errExit("MAP FAILED.\n");

	/* GUP */
	if (ioctl(fd, MM_GUP, (unsigned long)mem) < 0)
		errExit("IOCTL FAILED.\n");

	/* ACCESS */
	printf("GUP %#lx => %s\n", (unsigned long)mem, mem);

	/* RECLAIM */
	munmap(mem, MAP_SIZE);
	close(fd);

	return 0;
}
