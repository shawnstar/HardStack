// SPDX-License-Identifier: GPL-2.0
/*
 * BiscuitOS GUP AND CONTIGOUS MEMORY
 *
 * (C) 2024.04.10 BuddyZhang1 <buddy.zhang@aliyun.com>
 * (C) 2024.04.10 BiscuitOS
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define DEV_PATH		"/dev/BiscuitOS"
#define MAP_SIZE		(16 * 1024 * 1024) // 16MiB
#define FILE_PATH		"/mnt/ext4/BiscuitOS.txt"
#define O_DIRECT		00040000

int main()
{
	int fd, dfd;
	void *buffer;

	/* open device */
	fd = open(DEV_PATH, O_RDWR);
	if (fd < 0) {
		printf("ERROR: Can't open %s\n", DEV_PATH);
		return -1;
	}

	/* MMAP */
	buffer = mmap(NULL, MAP_SIZE,
		      PROT_READ | PROT_WRITE,
		      MAP_SHARED,
		      fd,
		      0);
	if (buffer == MAP_FAILED) {
		printf("ERROR: MAP_FAILED.\n");
		close(fd);
		return -1;
	}

	/* TEST */
	sprintf((char *)buffer, "Hello BiscuitOS");
	printf("TEST: %s\n", (char *)buffer);

	/* DIRECTIO */
	dfd = open(FILE_PATH, O_RDWR | O_DIRECT);
	if (dfd < 0) {
		printf("ERROR: Direct IO File.\n");
		munmap(buffer, MAP_SIZE);
		close(fd);
		return -1;
	}

	/* DIRECTIO Write */
	if (write(dfd, buffer, 4096) < 0) {
		printf("ERROR: Write FILE FAILED.\n");
		munmap(buffer, MAP_SIZE);
		close(fd);
		close(dfd);
		return -1;
	}

	munmap(buffer, MAP_SIZE);
	close(dfd);
	close(fd);

	printf("TEST SUCCED.\n");

	return 0;
}
