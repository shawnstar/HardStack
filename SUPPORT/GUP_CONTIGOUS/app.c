// SPDX-License-Identifier: GPL-2.0
/*
 * BiscuitOS GUP AND CONTIGOUS MEMORY
 *
 * (C) 2024.04.10 BuddyZhang1 <buddy.zhang@aliyun.com>
 * (C) 2024.04.10 BiscuitOS
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define DEV_PATH		"/dev/BiscuitOS"
#define MAP_SIZE		(16 * 1024 * 1024) // 16MiB

int main()
{
	int fd;
	void *buffer;

	/* open device */
	fd = open(DEV_PATH, O_RDWR);
	if (fd < 0) {
		printf("ERROR: Can't open %s\n", DEV_PATH);
		return -1;
	}

	/* MMAP */
	buffer = mmap(NULL, MAP_SIZE,
		      PROT_READ | PROT_WRITE,
		      MAP_SHARED,
		      fd,
		      0);
	if (buffer == MAP_FAILED) {
		printf("ERROR: MAP_FAILED.\n");
		close(fd);
		return -1;
	}

	/* TEST */
	sprintf((char *)buffer, "Hello BiscuitOS");
	printf("TEST: %s\n", (char *)buffer);

	/* FAKE Write */
	write(fd, buffer, 10);

	munmap(buffer, MAP_SIZE);
	close(fd);

	return 0;
}
