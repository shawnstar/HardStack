BiscuitOS GPU QEMU Module Usage
======================================

## Deploy

#### 1. Goto Qemu source code directory

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/qemu-system/
```

**VERSION** is kernel version and **ARCH** is architecture of platformat. qemu-system is soft link from variable version qemu.

#### 2. Create BiscuitOS Device Module directory

Create BiscuitOS directory on **hw/**  directory, then modify Makefile.objs and Kconfig, such as:

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/qemu-system/hw/
mkdir BiscuitOS
vi Makefile.objs

# Add context
devices-dirs-$(CONFIG_SOFTMMU) += BiscuitOS/

vi Kconfig

# Add context
source BiscuitOS/Kconfig
```

#### 3. Download module source code

We need download and copy source code from BiscuitOS, sach as:

```
cd BiscuitOS/
make linux-6.0-x86_64_defconfig
make menuconfig 

  [*] DIY BiscuitOS/Broiler Hardware  --->
      [*]   BiscuitOS GPU (VRAM on BAR)
  [*] Package --->
      [*] GPU: Graphics Processing Unit --->
          -*- GPU DD(VRAM on BAR) --->
          -*- QEMU GPU DEVICE: VRAM on BAR--->

make
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/package/BiscuitOS-QEMU-GPU-VRAM-BAR-default/
make download
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/qemu-system/hw/BiscuitOS/
cp BiscuitOS/output/linux-${VERSION}-${ARCH}/package/BiscuitOS-QEMU-GPU-VRAM-BAR-default/BiscuitOS-QEMU-GPU-VRAM-BAR-default ./ -rf
```

And the modify Makefile.objs and Kconfig under **hw/BiscuitOS/** directory, such as:

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/qemu-system/hw/BiscuitOS/
vi Makefile.objs

# Add context
common-obj-$(CONFIG_BISCUITOS_GPU_VRAM_BAR) += BiscuitOS-QEMU-GPU-VRAM-BAR-default/

vi Kconfig

# Add context
source BiscuitOS-QEMU-GPU-VRAM-BAR-default/Kconfig
```

#### 4. Enable BiscuitOS Macro

We need create some macro to enable BiscuitOS module, **CONFIG_BISCUITOS_GPU_VRAM_BAR** macro is used to enable BiscuitOS PCI module, **CONFIG_BISCUITOS_DMA** macro is used to enable BiscuitOS DMA module. We need enable those on special file, such as on X86 Emulator:

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/qemu-system/
vi default-configs/i386-softmmu.mak

# Add context
CONFIG_BISCUITOS_GPU_VRAM_BAR=y

vi config-all-devices.mak

# Add context
CONFIG_BISCUITOS_GPU_VRAM_BAR:=$(findstring y,$(CONFIG_BISCUITOS_GPU_VRAM_BAR)y)
```

#### 5. Add QEMU command

The BiscuitOS PCI module is called from QEMU command, so we need add qemu options for module, such as:

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/
vi RunBiscuitOS.sh

# Check and if doesn't exist, then add context
        -hda ${ROOT}/BiscuitOS.img \
+       -device BiscuitOS-GPU-VRAM-BAR \
        -drive file=${ROOT}/Freeze.img,if=virtio \
```

The name of PCI module is **BiscuitOS-GPU-VRAM-BAR**, the options "-device" while invoke on QEMU.

#### 6. Compile QEMU

BiscuitOS provides a auto scripts to compile QEMU, such as:

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/
./RunQEMU.sh -b
```

The BiscuitOS is running when compile pass, and the BiscuitOS-pci module is running.

```
 ____  _                _ _    ___  ____  
| __ )(_)___  ___ _   _(_) |_ / _ \/ ___| 
|  _ \| / __|/ __| | | | | __| | | \___ \ 
| |_) | \__ \ (__| |_| | | |_| |_| |___) |
|____/|_|___/\___|\__,_|_|\__|\___/|____/ 
Welcome to BiscuitOS

Please press Enter to activate this console. 
~ # dmesg | grep 1026
[    0.216614] pci 0000:00:04.0: [1010:1991] type 00 class 0x00ff00
~ # 
~ #
```

The ID 1010:1413 is BiscuitOS-PCI-DMA-MSIX device. CONFIG_BISCUITOS_GPU_VRAM_BAR is enable.

## File

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/qemu-system/qemu-system/hw/BiscuitOS/BiscuitOS-QEMU-GPU-VRAM-BAR-default
tree

├── main.c
├── Kconfig
├── Makefile.objs
└── README.md
```

**main.c** is source code for BiscuitOS GPU module, and Kconfig create macro BISCUITOS_GPU_VRAM_BAR, Makefile.objs add it to compiler. the usage of QEMU from README.md.

## Running Device Driver

The end, Running Device Driver on Guest, such as:

```
cd BiscuitOS/output/linux-${VERSION}-${ARCH}/package/BiscuitOS-GPU-VRAM-BAR
make download
make build


# The BiscuitOS Running:
 ____  _                _ _    ___  ____
| __ )(_)___  ___ _   _(_) |_ / _ \/ ___|
|  _ \| / __|/ __| | | | | __| | | \___ \
| |_) | \__ \ (__| |_| | | |_| |_| |___) |
|____/|_|___/\___|\__,_|_|\__|\___/|____/
Welcome to BiscuitOS

Please press Enter to activate this console.
/ # insmod /lib/modules/$(uname -r)/extra/BiscuitOS-GPU-VRAM-BAR-default.ko
[   31.191134] Broiler_dma_msix_default: loading out-of-tree module taints kernel.
[    2.217888] Broiler-PCI-DMA-MSIX Success Register PCIe Device.
[    2.218155] DMA Buffer[Weclome to BiscuitOS/Broiler  :)]
```

## Blog

```
http://www.biscuitos.cn/blog/BiscuitOS_Catalogue/
```
